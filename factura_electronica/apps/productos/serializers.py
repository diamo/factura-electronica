# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import ProductoComprobante


class ProductoComprobanteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductoComprobante
        fields = ('codigo', 'cod_mtx', 'descripcion', 'unidad', 'cantidad',
                  'precio_vta_sin_iva', 'tasa_iva', 'porcentaje_descuento')

