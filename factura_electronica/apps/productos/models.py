# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from factura_electronica.apps.referencias.models import TasaIVA, UnidadMedida
from factura_electronica.apps.comprobantes.models import Comprobante


class ProductoComprobante(models.Model):

    comprobante = models.ForeignKey(Comprobante, related_name="productos")
    cod_mtx = models.CharField(max_length=13, default="1", help_text="Default Ventas varias")
    codigo = models.CharField(max_length=13)
    descripcion = models.CharField(max_length=500)
    unidad = models.ForeignKey(UnidadMedida)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2)
    precio_vta_sin_iva = models.DecimalField(max_digits=10, decimal_places=2)
    tasa_iva = models.ForeignKey(TasaIVA)
    porcentaje_descuento = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True,
                                               validators=[MinValueValidator(0.00), MaxValueValidator(100.00)])

    class Meta:
        verbose_name = "Producto Comprobante"
        verbose_name_plural = "Productos Comprobante"

    @property
    def subtotal(self):
        return self.cantidad * self.precio_vta_sin_iva

    @property
    def iva(self):
        return (self.subtotal * self.tasa_iva.porcentaje) / 100

    @property
    def total(self):
        return self.subtotal + self.iva
