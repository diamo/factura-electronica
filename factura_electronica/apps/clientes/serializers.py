# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import Cliente


class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = ('tipo_documento', 'nro_documento', 'razon_social', 'tipo_iva')

