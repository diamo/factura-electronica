# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from factura_electronica.apps.referencias.models import TipoDocumento, TipoIVA


class Cliente(models.Model):

    tipo_documento = models.ForeignKey(TipoDocumento, null=True, blank=True)
    nro_documento = models.CharField(max_length=11, null=True, blank=True)
    razon_social = models.CharField(max_length=150, null=True, blank=True)
    tipo_iva = models.ForeignKey(TipoIVA)

    class Meta:
        verbose_name = "Cliente Comprobante"
        verbose_name_plural = "Clientes Comprobante"
