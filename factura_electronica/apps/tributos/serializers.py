# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import TributoComprobante, IvaComprobante


class TributoComprobanteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TributoComprobante
        fields = ('tipo_impuesto', 'descripcion_impuesto', 'base_impuesto', 'alicuota_impuesto')


class IvaComprobanteSerializer(serializers.ModelSerializer):
    class Meta:
        model = IvaComprobante
        fields = ('tasa_iva', 'base_impuesto')
