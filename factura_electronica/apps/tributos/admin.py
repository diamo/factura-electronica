# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import TributoComprobante


@admin.register(TributoComprobante)
class TributoComprobanteAdmin(admin.ModelAdmin):
    pass
