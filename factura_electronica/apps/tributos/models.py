# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from factura_electronica.apps.referencias.models import TipoImpuesto, TasaIVA
from factura_electronica.apps.comprobantes.models import Comprobante


class TributoComprobante(models.Model):

    comprobante = models.ForeignKey(Comprobante, related_name="tributos")
    tipo_impuesto = models.ForeignKey(TipoImpuesto)
    descripcion_impuesto = models.CharField(max_length=500, null=True, blank=True)
    base_impuesto = models.DecimalField(max_digits=10, decimal_places=2)
    alicuota_impuesto = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        verbose_name = "Tributo Comprobante"
        verbose_name_plural = "Tributos Comprobante"

    @property
    def importe(self):
        return (self.base_impuesto * self.alicuota_impuesto) / 100


class IvaComprobante(models.Model):

    comprobante = models.ForeignKey(Comprobante, related_name="ivas")
    tasa_iva = models.ForeignKey(TasaIVA)
    base_impuesto = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        verbose_name = "Iva Comprobante"
        verbose_name_plural = "Iva Comprobante"

    @property
    def importe(self):
        return (self.base_impuesto * self.tasa_iva.porcentaje) / 100
