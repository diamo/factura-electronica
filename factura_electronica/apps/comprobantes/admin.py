# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Comprobante, ComprobanteAsociado


@admin.register(Comprobante)
class ComprobanteAdmin(admin.ModelAdmin):
    pass


@admin.register(ComprobanteAsociado)
class ComprobanteAsociadoAdmin(admin.ModelAdmin):
    pass
