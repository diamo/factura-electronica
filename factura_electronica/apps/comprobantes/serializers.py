# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import ComprobanteAsociado, Comprobante
from factura_electronica.apps.tributos.models import TributoComprobante, IvaComprobante
from factura_electronica.apps.productos.models import ProductoComprobante
from factura_electronica.apps.clientes.models import Cliente
from factura_electronica.apps.referencias.models import TipoComprobante, Concepto
from factura_electronica.apps.clientes.serializers import ClienteSerializer
from factura_electronica.apps.tributos.serializers import TributoComprobanteSerializer, IvaComprobanteSerializer
from factura_electronica.apps.productos.serializers import ProductoComprobanteSerializer


class ComprobanteAsociadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComprobanteAsociado
        fields = ('punto_venta', 'numero')


class ComprobanteSerializer(serializers.ModelSerializer):

    tipo_comprobante = serializers.PrimaryKeyRelatedField(queryset=TipoComprobante.objects.all())
    concepto = serializers.PrimaryKeyRelatedField(queryset=Concepto.objects.all())
    cliente = ClienteSerializer()
    tributos = TributoComprobanteSerializer(many=True, required=False)
    ivas = IvaComprobanteSerializer(many=True, required=False)
    productos = ProductoComprobanteSerializer(many=True, required=False)
    cbtes_asoc = ComprobanteAsociadoSerializer(many=True, required=False)

    class Meta:
        model = Comprobante
        fields = ('service', 'certificate', 'private_key', 'modo', 'fecha', 'punto_venta', 'tipo_comprobante', 'ivas',
                  'concepto', 'fecha_servicio_desde', 'fecha_servicio_hasta', 'fecha_vencimiento_pago',
                  'cliente', 'tributos', 'productos', 'cbtes_asoc', 'cuit')

    def validate(self, data):
        """
        Sólo concepto 2 y 3 fechas de servicio y vencimiento
        """
        concepto = data['concepto'].clave
        if concepto == '2' or concepto == '3':
            if ('fecha_servicio_desde' not in data) or ('fecha_servicio_hasta' not in data) or \
               ('fecha_vencimiento_pago' not in data):
                raise serializers.ValidationError("fecha_servicio_desde, fecha_servicio_hasta,\
                                                  fecha_vencimiento_pago requeridos para concepto 2 o 3")
        else:
            data['fecha_vencimiento_pago'] = None
            data['fecha_servicio_desde'] = None
            data['fecha_servicio_hasta'] = None
        return data

    def create(self, validated_data):
        cliente_data = validated_data.pop('cliente')
        tributos_data = validated_data.pop('tributos')
        ivas_data = validated_data.pop('ivas')
        productos_data = validated_data.pop('productos')
        cbtes_asoc_data = validated_data.pop('cbtes_asoc')

        validated_data['cliente'] = Cliente.objects.create(**cliente_data)
        comprobante = Comprobante.objects.create(**validated_data)

        for tributo_data in tributos_data:
            TributoComprobante.objects.create(comprobante=comprobante, **tributo_data)

        for iva_data in ivas_data:
            IvaComprobante.objects.create(comprobante=comprobante, **iva_data)

        for producto_data in productos_data:
            ProductoComprobante.objects.create(comprobante=comprobante, **producto_data)

        for cbte_asoc_data in cbtes_asoc_data:
            ComprobanteAsociado.objects.create(comprobante=comprobante, **cbte_asoc_data)

        return comprobante
