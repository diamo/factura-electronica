# -*- coding: utf-8 -*-

from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from .models import Comprobante
from .serializers import ComprobanteSerializer


class ComprobanteCreateViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):

    queryset = Comprobante.objects.all()
    serializer_class = ComprobanteSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        response = autorizar_comprobante(obj)
        return Response(response, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        obj = serializer.save()
        return obj


def autorizar_comprobante(comprobante):
    result, auth_data = comprobante.pyafipws_authenticate()
    if result is False:
        return auth_data

    ws, wsdl = comprobante.get_ws_wsdl()
    comprobante.connect_set_credentials(auth_data, ws, wsdl)
    comprobante.create_invoice(ws)
    comprobante.add_tributos(ws)
    comprobante.add_iva(ws)
    comprobante.add_productos(ws)
    error = comprobante.request_authorization(ws)
    if error is not True:
        erorres = error.split('\n')
        return erorres

    comprobante.calculate_barcode(ws)
    resultado, obj = comprobante.set_cae(ws)

    if resultado is not True:
        return obj

    response = {
        "CAE": obj.pyafipws_cae,
        "fecha_vto_cae": obj.pyafipws_cae_due_date,
        "importe_total": ws.factura['imp_total'],
        "barcode": obj.pyafipws_barcode,
        "observaciones_afip": ws.Obs,
        "xml_request": ws.XmlRequest,
        "xml_response": ws.XmlResponse,
    }

    comprobante.delete()
    return response
