# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from decimal import Decimal
import traceback
import sys

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from factura_electronica.apps.referencias.models import TipoComprobante, Concepto
from factura_electronica.apps.clientes.models import Cliente
from factura_electronica.core import afip_auth


class Comprobante(models.Model):

    PYAFIPWS_ELECTRONIC_INVOICE_SERVICE = [
        ('', ''),
        ('wsfe', u'Mercado interno -sin detalle- RG2485 (WSFEv1)'),
        ('wsmtxca', u'Mercado interno -con detalle- RG2904 (WSMTXCA)'),
        ('wsbfe', u'Bono Fiscal -con detalle- RG2557 (WSMTXCA)'),
        ('wsfex', u'Exportación -con detalle- RG2758 (WSFEXv1)'),
    ]

    MODE_CERT = [
        ('homologacion', u'Homologación'),
        ('produccion', u'Producción'),
    ]

    service = models.CharField(max_length=10, choices=PYAFIPWS_ELECTRONIC_INVOICE_SERVICE)
    certificate = models.TextField()
    private_key = models.TextField()
    modo = models.CharField(max_length=12, choices=MODE_CERT)
    fecha = models.DateField()
    punto_venta = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(9998)])
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    concepto = models.ForeignKey(Concepto)
    fecha_servicio_desde = models.DateField(null=True, blank=True)
    fecha_servicio_hasta = models.DateField(null=True, blank=True)
    fecha_vencimiento_pago = models.DateField(null=True, blank=True)
    cuit = models.CharField(max_length=11)
    cliente = models.ForeignKey(Cliente)

    class Meta:
        verbose_name = "Comprobante"
        verbose_name_plural = "Comprobantes"

    def get_ws_wsdl(self):
        """
        Devuelve una instancia del ws de AFIP a utilizar
        """
        if self.service == 'wsfe':
            from factura_electronica.core.pyafipws.wsfev1 import WSFEv1  # Local Market
            ws = WSFEv1()
            if self.modo == 'homologacion':
                wsdl = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL"
            else:
                wsdl = "https://servicios1.afip.gov.ar/wsfev1/service.asmx?WSDL"
            return ws, wsdl
        elif self.service == 'wsmtxca':
            from factura_electronica.core.pyafipws.wsmtx import WSMTXCA  # Local + Detail
            ws = WSMTXCA()
            if self.modo == 'homologacion':
                wsdl = "https://fwshomo.afip.gov.ar/wsmtxca/services/MTXCAService?wsdl"
            else:
                wsdl = "https://serviciosjava.afip.gob.ar/wsmtxca/services/MTXCAService?wsdl"
            return ws, wsdl
        elif self.service == 'wsfex':
            from factura_electronica.core.pyafipws.wsfexv1 import WSFEXv1
            ws = WSFEXv1()
            if self.modo == 'homologacion':
                wsdl = "https://wswhomo.afip.gov.ar/wsfexv1/service.asmx?WSDL"
            else:
                wsdl = "https://servicios1.afip.gov.ar/wsfexv1/service.asmx?WSDL"
            return ws, wsdl
        else:
            return "Service %s no soportado" % (self.service), None

    def pyafipws_authenticate(self, force=False):
        """Authenticate against AFIP, returns token, sign, err_msg (dict)"""
        auth_data = {}
        if self.modo == 'homologacion':
            WSAA_URL = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?wsdl"
        else:
            WSAA_URL = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl"

        # Call the helper function to obtain the access ticket:
        auth = afip_auth.authenticate(self.service, self.certificate, self.private_key, wsdl=WSAA_URL, force=force)
        auth_data.update(auth)
        if auth_data['err_msg']:
            return False, "Error Auth: %s" % (auth_data['err_msg'])
        return True, auth_data

    def connect_set_credentials(self, auth_data, ws, wsdl):
        """
        Connect to the Webservice and set AFIP Credentials
        """
        ws.LanzarExcepciones = True
        cache_dir = afip_auth.get_cache_dir()
        ws.Conectar(wsdl=wsdl, cache=cache_dir)

        # Set AFIP webservice credentials:
        ws.Cuit = self.cuit
        ws.Token = auth_data['token']
        ws.Sign = auth_data['sign']

    def get_last_invoice_number(self, ws):
        """
        Get last invoice number registered in AFIP
        """
        if self.service == 'wsfe':
            cbte_nro_afip = ws.CompUltimoAutorizado(self.tipo_comprobante.clave, self.punto_venta)
        if self.service == 'wsmtxca':
            cbte_nro_afip = ws.ConsultarUltimoComprobanteAutorizado(self.tipo_comprobante.clave, self.punto_venta)
        elif self.service == 'wsfex':
            cbte_nro_afip = ws.GetLastCMP(self.tipo_comprobante.clave, self.punto_venta)
        return cbte_nro_afip

    def check_invoice_number(self, cbte_nro, cbte_nro_afip):
        """
        Verify that the invoice is the next one to be registered in AFIP
        TODO - Ver si usarlo, serviria si pedimos el cbt_nro por la api
        y de ser asi seria if self.cbte_nro.
        """
        cbte_nro_next = int(cbte_nro_afip or 0) + 1
        if cbte_nro != cbte_nro_next:
            return False, 'Error! El número del comprobante debería ser %s y no %s' % (str(cbte_nro_next), str(cbte_nro))
        return True, ''

    def get_imp_total_iva(self):
        imp_iva = Decimal('0')
        if self.service in ('wsfe', 'wsmtxca'):
            for iva in self.ivas.all():
                imp_iva += iva.importe
        return abs(imp_iva)

    def get_imp_total_tributo(self):
        imp_trib = Decimal('0')
        if self.service in ('wsfe', 'wsmtxca'):
            for tributo in self.tributos.all():
                imp_trib += tributo.importe
        return abs(imp_trib)

    def get_imp_total_productos_sin_iva(self):
        imp_prod = Decimal('0')
        if self.service in ('wsmtxca'):
            for producto in self.productos.all():
                imp_prod += producto.subtotal
        return abs(imp_prod)

    def get_imp_total(self):
        return self.get_imp_total_productos_sin_iva() + self.get_imp_total_tributo() + self.get_imp_total_iva()

    def create_invoice(self, ws):
        cbte_nro_afip = self.get_last_invoice_number(ws)
        cbte_nro_next = int(cbte_nro_afip or 0) + 1
        cbt_desde = cbt_hasta = cbte_nro_next

        imp_neto = "%.2f" % (self.get_imp_total_productos_sin_iva())
        imp_iva = "%.2f" % (self.get_imp_total_iva())
        imp_trib = "%.2f" % (self.get_imp_total_tributo())
        imp_subtotal = imp_neto
        imp_total = "%.2f" % (self.get_imp_total())

        imp_tot_conc = "0.00"
        imp_op_ex = "0.00"

        moneda_id = 'PES'  # TODO
        moneda_ctz = 1  # TODO
        obs_generales = ''  # TODO

        if self.service == 'wsfe':
            fecha = str(self.fecha).replace("-", "")
            fecha_servicio_desde = str(self.fecha_servicio_desde).replace("-", "")
            fecha_servicio_hasta = str(self.fecha_servicio_hasta).replace("-", "")
            fecha_vencimiento_pago = str(self.fecha_vencimiento_pago).replace("-", "")
            ws.CrearFactura(self.concepto.clave, self.cliente.tipo_documento.clave,
                            self.cliente.nro_documento, self.tipo_comprobante.clave,
                            self.punto_venta, cbt_desde, cbt_hasta, imp_total, imp_tot_conc, imp_neto,
                            imp_iva, imp_trib, imp_op_ex, fecha, fecha_vencimiento_pago,
                            fecha_servicio_desde, fecha_servicio_hasta, moneda_id, moneda_ctz)
        elif self.service == 'wsmtxca':
            ws.CrearFactura(self.concepto.clave, self.cliente.tipo_documento.clave,
                            self.cliente.nro_documento, self.tipo_comprobante.clave,
                            self.punto_venta, cbt_desde, cbt_hasta, imp_total, imp_tot_conc, imp_neto,
                            imp_subtotal, imp_trib, imp_op_ex, self.fecha, self.fecha_vencimiento_pago,
                            self.fecha_servicio_desde, self.fecha_servicio_hasta, moneda_id, moneda_ctz, obs_generales)
        elif self.service == 'wsfex':  # TODO
            pass

    def add_tributos(self, ws):
        for tributo in self.tributos.all():
            importe = "%.2f" % (tributo.importe)
            ws.AgregarTributo(tributo.tipo_impuesto.clave, tributo.descripcion_impuesto, tributo.base_impuesto,
                              tributo.alicuota_impuesto, importe)

    def add_iva(self, ws):
        for iva in self.ivas.all():
            importe = "%.2f" % (iva.importe)
            ws.AgregarIva(iva.tasa_iva.clave, iva.base_impuesto, importe)

    def add_productos(self, ws):
        """
        TODO: revisar bien como hacer con u_mtx y cod_mtx
        http://www.sistemasagiles.com.ar/trac/wiki/FacturaElectronicaMTXCAService
        """
        if self.service in ('wsfex', 'wsmtxca'):
            for prod in self.productos.all():
                u_mtx = 1
                cod_mtx = 1  # Si no da cod_mtx debe ser 1 y u_mtx también
                imp_iva = "%.2f" % (prod.iva)
                imp_subtotal = "%.2f" % (prod.total)
                ws.AgregarItem(u_mtx, cod_mtx, prod.codigo, prod.descripcion, prod.cantidad,
                               prod.unidad.clave, prod.precio_vta_sin_iva, prod.porcentaje_descuento,
                               prod.tasa_iva.clave, imp_iva, imp_subtotal)

    def add_permisos(self, ws):
        # TODO
        self.licences = "TODO"
        if self.service == 'wsfex':
            for export_license in self.licences:
                ws.AgregarPermiso(export_license.license_id, export_license.country)

    def request_authorization(self, ws):
        """
        Request the authorization! (call the AFIP webservice method)
        """
        try:
            if self.service == 'wsfe':
                ws.CAESolicitar()
                self.vto = ws.Vencimiento
            elif self.service == 'wsmtxca':
                ws.AutorizarComprobante()
                self.vto = ws.Vencimiento
            elif self.service == 'wsfex':
                ws.Authorize(self.id)
                self.vto = ws.FchVencCAE
        except Exception, e:
            if ws.Excepcion:
                # Get the exception already parsed by the helper
                return ws.Excepcion + ' ' + str(e)
            else:
                # Avoid encoding problem when reporting exceptions to the user:
                return traceback.format_exception_only(sys.exc_type, sys.exc_value)[0]
        else:
            if ws.Obs or ws.ErrMsg:
                return u"\n".join([ws.Obs or "", ws.ErrMsg or ""])
        return True

    def calculate_barcode(self, ws):
        """
        Calculate the barcode:
        """
        self.bars = ""
        if ws.CAE:
            cae_due = ''.join([c for c in str(ws.Vencimiento or '') if c.isdigit()])
            bars = ''.join([str(ws.Cuit), "%02d" % int(self.tipo_comprobante.clave),
                           "%04d" % int(self.punto_venta), str(ws.CAE), cae_due])
            self.bars = bars + self.pyafipws_verification_digit_modulo10(bars)
        return self.bars

    def set_cae(self, ws):
        if ws.CAE:
            # store the results
            self.pyafipws_cae = ws.CAE
            self.pyafipws_cae_due_date = self.vto or None
            self.pyafipws_barcode = self.bars

            if '-' not in self.pyafipws_cae_due_date:
                fe = self.pyafipws_cae_due_date
                self.pyafipws_cae_due_date = '-'.join([fe[:4], fe[4:6], fe[6:8]])
            return True, self
        else:
            return False, 'Error cae'

    def pyafipws_verification_digit_modulo10(self, codigo):
        "Calculate the verification digit 'modulo 10'"
        # http://www.consejo.org.ar/Bib_elect/diciembre04_CT/documentos/rafip1702.htm
        # Step 1: sum all digits in odd positions, left to right
        codigo = codigo.strip()
        if not codigo or not codigo.isdigit():
            return ''
        etapa1 = sum([int(c) for i, c in enumerate(codigo) if not i % 2])
        # Step 2: multiply the step 1 sum by 3
        etapa2 = etapa1 * 3
        # Step 3: start from the left, sum all the digits in even positions
        etapa3 = sum([int(c) for i, c in enumerate(codigo) if i % 2])
        # Step 4: sum the results of step 2 and 3
        etapa4 = etapa2 + etapa3
        # Step 5: the minimun value that summed to step 4 is a multiple of 10
        digito = 10 - (etapa4 - (int(etapa4 / 10) * 10))
        if digito == 10:
            digito = 0
        return str(digito)


class ComprobanteAsociado(models.Model):

    comprobante = models.ForeignKey(Comprobante, related_name="cbtes_asoc")
    punto_venta = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(9998)])
    numero = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(99999998)])

    class Meta:
        verbose_name = "Comprobante Asociado"
        verbose_name_plural = "Comprobantes Asociados"
