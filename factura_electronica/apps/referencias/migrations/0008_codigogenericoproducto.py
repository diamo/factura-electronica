# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-21 18:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('referencias', '0007_tasaiva_porcentaje'),
    ]

    operations = [
        migrations.CreateModel(
            name='CodigoGenericoProducto',
            fields=[
                ('clave', models.CharField(max_length=15, primary_key=True, serialize=False)),
                ('valor', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name': 'Codigo generico de Producto',
                'verbose_name_plural': 'Codigos genericos de Producto',
            },
        ),
    ]
