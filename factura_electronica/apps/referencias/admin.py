# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import TipoDocumento, TipoComprobante, Concepto, TasaIVA, TipoImpuesto, CondicionVenta, TipoIVA
from .models import Moneda, UnidadMedida, CodigoGenericoProducto


@admin.register(CodigoGenericoProducto)
class CodigoGenericoProductoAdmin(admin.ModelAdmin):
    pass


@admin.register(Moneda)
class MonedaAdmin(admin.ModelAdmin):
    pass


@admin.register(UnidadMedida)
class UnidadMedidaAdmin(admin.ModelAdmin):
    pass


@admin.register(TipoDocumento)
class TipoDocumentoAdmin(admin.ModelAdmin):
    pass


@admin.register(TipoComprobante)
class TipoComprobanteAdmin(admin.ModelAdmin):
    pass


@admin.register(Concepto)
class ConceptoAdmin(admin.ModelAdmin):
    pass


@admin.register(TasaIVA)
class TasaIVAAdmin(admin.ModelAdmin):
    pass


@admin.register(TipoIVA)
class TipoIVAAdmin(admin.ModelAdmin):
    pass


@admin.register(TipoImpuesto)
class TipoImpuestoAdmin(admin.ModelAdmin):
    pass


@admin.register(CondicionVenta)
class CondicionVentaAdmin(admin.ModelAdmin):
    pass
