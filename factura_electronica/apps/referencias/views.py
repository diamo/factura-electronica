# -*- coding: utf-8 -*-

from rest_framework import mixins
from rest_framework import viewsets

from .models import TipoDocumento, TipoComprobante, Concepto, TasaIVA, TipoImpuesto, CondicionVenta, TipoIVA
from .models import Moneda, UnidadMedida
from .serializers import TipoDocumentoSerializer, TipoComprobanteSerializer, ConceptoSerializer, TasaIVASerializer
from .serializers import TipoImpuestoSerializer, CondicionVentaSerializer, TipoIVASerializer
from .serializers import UnidadMedidaSerializer, MonedaSerializer


class UnidadMedidaList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                       viewsets.GenericViewSet):

    queryset = UnidadMedida.objects.all()
    serializer_class = UnidadMedidaSerializer


class MonedaList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                 viewsets.GenericViewSet):

    queryset = Moneda.objects.all()
    serializer_class = MonedaSerializer


class TipoDocumentoList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                        viewsets.GenericViewSet):

    queryset = TipoDocumento.objects.all()
    serializer_class = TipoDocumentoSerializer


class TipoComprobanteList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                          viewsets.GenericViewSet):

    queryset = TipoComprobante.objects.all()
    serializer_class = TipoComprobanteSerializer


class ConceptoList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                   viewsets.GenericViewSet):

    queryset = Concepto.objects.all()
    serializer_class = ConceptoSerializer


class TasaIVAList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                  viewsets.GenericViewSet):

    queryset = TasaIVA.objects.all()
    serializer_class = TasaIVASerializer


class TipoIVAList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                  viewsets.GenericViewSet):

    queryset = TipoIVA.objects.all()
    serializer_class = TipoIVASerializer


class TipoImpuestoList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                       viewsets.GenericViewSet):

    queryset = TipoImpuesto.objects.all()
    serializer_class = TipoImpuestoSerializer


class CondicionVentaList(mixins.RetrieveModelMixin, mixins.ListModelMixin,
                         viewsets.GenericViewSet):

    queryset = CondicionVenta.objects.all()
    serializer_class = CondicionVentaSerializer
