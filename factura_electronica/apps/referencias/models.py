# -*- coding: utf-8 -*-

from django.utils.encoding import smart_unicode

from django.db import models


class Referencia(models.Model):

    clave = models.CharField(max_length=15, primary_key=True)
    valor = models.CharField(max_length=150)

    def __unicode__(self):
        return smart_unicode(u'{} - {}'.format(self.clave, self.valor))

    class Meta:
        abstract = True


class TipoDocumento(Referencia):

    class Meta:
        verbose_name = "Tipo de Documento"
        verbose_name_plural = "Tipos de Documentos"


class TipoComprobante(Referencia):

    class Meta:
        verbose_name = "Tipo de Comprobante"
        verbose_name_plural = "Tipos de Comprobantes"


class Concepto(Referencia):

    class Meta:
        verbose_name = "Concepto"
        verbose_name_plural = "Conceptos"


class TasaIVA(Referencia):

    porcentaje = models.DecimalField(max_digits=4, decimal_places=2, default=0)

    class Meta:
        verbose_name = "Tasa de IVA"
        verbose_name_plural = "Tasas de IVA"


class TipoIVA(Referencia):

    class Meta:
        verbose_name = "Tipo de IVA"
        verbose_name_plural = "Tipos de IVA"


class TipoImpuesto(Referencia):

    class Meta:
        verbose_name = "Tipo de Impuesto"
        verbose_name_plural = "Tipos de Impuestos"


class CondicionVenta(Referencia):

    class Meta:
        verbose_name = "Condicion de Venta"
        verbose_name_plural = "Condiciones de Venta"


class Moneda(Referencia):

    class Meta:
        verbose_name = "Moneda"
        verbose_name_plural = "Monedas"


class UnidadMedida(Referencia):

    class Meta:
        verbose_name = "Unidad de Medida"
        verbose_name_plural = "Unidades de Medida"


class CodigoGenericoProducto(Referencia):

    class Meta:
        verbose_name = "Codigo generico de Producto"
        verbose_name_plural = "Codigos genericos de Producto"
