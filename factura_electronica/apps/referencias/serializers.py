# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import TipoDocumento, TipoComprobante, Concepto, TasaIVA, TipoImpuesto, CondicionVenta, TipoIVA
from .models import UnidadMedida, Moneda, CodigoGenericoProducto


class CodigoGenericoProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CodigoGenericoProducto
        fields = ('clave', 'valor')


class UnidadMedidaSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnidadMedida
        fields = ('clave', 'valor')


class MonedaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Moneda
        fields = ('clave', 'valor')


class TipoDocumentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoDocumento
        fields = ('clave', 'valor')


class TipoComprobanteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoComprobante
        fields = ('clave', 'valor')


class ConceptoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Concepto
        fields = ('clave', 'valor')


class TasaIVASerializer(serializers.ModelSerializer):
    class Meta:
        model = TasaIVA
        fields = ('clave', 'valor', 'porcentaje')


class TipoImpuestoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoImpuesto
        fields = ('clave', 'valor')


class CondicionVentaSerializer(serializers.ModelSerializer):
    class Meta:
        model = CondicionVenta
        fields = ('clave', 'valor')


class TipoIVASerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoIVA
        fields = ('clave', 'valor')
