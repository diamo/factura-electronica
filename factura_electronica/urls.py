#! -*- coding: utf8 -*-
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import routers

from factura_electronica.apps.referencias import views
from factura_electronica.apps.comprobantes import views as comprobantes_views

router = routers.DefaultRouter()
router.register(r'referencias/tipo-documento', views.TipoDocumentoList, 'tipo_documento')
router.register(r'referencias/tipo-comprobante', views.TipoComprobanteList, 'tipo_comprobante')
router.register(r'referencias/concepto', views.ConceptoList, 'concepto')
router.register(r'referencias/tasa-iva', views.TasaIVAList, 'tasa_iva')
router.register(r'referencias/tipo-iva', views.TipoIVAList, 'tipo_iva')
router.register(r'referencias/tipo-impuesto', views.TipoImpuestoList, 'tipo_impuesto')
router.register(r'referencias/condicion-venta', views.CondicionVentaList, 'condicion_venta')
router.register(r'referencias/moneda', views.MonedaList, 'moneda')
router.register(r'referencias/unidad-medida', views.UnidadMedidaList, 'unidad_medida')

router.register(r'comprobante', comprobantes_views.ComprobanteCreateViewSet, 'comprobante_create')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(router.urls)),
]
